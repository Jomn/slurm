#!/usr/bin/env python
# coding: utf-8

import argparse
import logging
import sys
from enum import Enum
from typing import List, IO

# from pytools.helpers.argutils import EnumAction  # type: ignore

class EnumAction(argparse.Action):
    """
    Argparse action for handling Enums
    """
    def __init__(self, **kwargs):
        # Pop off the type value
        enum = kwargs.pop("type", None)

        # Ensure an Enum subclass is provided
        if enum is None:
            raise ValueError("type must be assigned an Enum when using EnumAction")
        if not issubclass(enum, Enum):
            raise TypeError("type must be an Enum when using EnumAction")

        # Generate choices from the Enum
        kwargs.setdefault("choices", tuple(e.value for e in enum))

        super(EnumAction, self).__init__(**kwargs)

        self._enum = enum

    def __call__(self, parser, namespace, values, option_string=None):
        # Convert value back into an Enum
        enum = self._enum(values)
        setattr(namespace, self.dest, enum)


class Partition(Enum):
    GPU = "gpu"
    CPU = "cpu"
    ALL = "all"


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('job_name', type=str, help="Name of the job (also used for output files)")
    parser.add_argument('--output-dir', '-o', type=str, help="Output directory for stdout and stderr ")
    parser.add_argument('--gpu', '-g', nargs="?", type=int, default=0, const=1,
                        help="Use a GPU (can also specify the amount needed)")
    parser.add_argument('--walltime', '-t', default="01:00:00", type=str)
    parser.add_argument('--partition', '-p', default=Partition.ALL, type=Partition, action=EnumAction)
    parser.add_argument('--constraint', '-c', type=str)
    parser.add_argument('--hosts', '-H', type=str, help="List of hosts/nodes to use (comma-separated).")
    parser.add_argument('--array', '-A', type=str, help="See https://slurm.schedmd.com/job_array.html")
    parser.add_argument('--cuda-home', '-C', default="/usr/local/cuda-11.0", type=str)
    parser.add_argument('--python-location', '-P', type=str)
    parser.add_argument('-l', '--logger', default='INFO',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help="Logging level: DEBUG, INFO (default), WARNING, ERROR")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.logger.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.logger))
    logging.basicConfig(level=numeric_level)

    return args


def setup_parameters(args: argparse.Namespace) -> List[str]:
    is_array_job: bool = args.array is not None
    parameters: List[str] = []

    parameters.append(f"#SBATCH -J {args.job_name}")
    parameters.append(f"#SBATCH -t {args.walltime}")
    parameters.append(f"#SBATCH -p {args.partition.value}")

    if args.gpu > 0:
        if args.partition not in [Partition.ALL, Partition.GPU]:
            raise RuntimeError("Inconsistent arguments: Requesting partition without GPUs")
        parameters.append(f"#SBATCH --gpus {args.gpu}")

    if args.constraint is not None:
        parameters.append(f"#SBATCH --constraint={args.constraint}")

    if args.hosts is not None:
        parameters.append(f"#SBATCH -w {args.hosts}")

    if is_array_job:
        parameters.append(f"#SBATCH --array={args.array}")

    job_id: str = "%j" if not is_array_job else "%A_%a"
    output_dir: str = "." if args.output_dir is None else args.output_dir
    parameters.append(f"#SBATCH -o {output_dir}/{args.job_name}.%N.{job_id}.out")
    parameters.append(f"#SBATCH -e {output_dir}/{args.job_name}.%N.{job_id}.err")

    return parameters


def setup_base_content(args: argparse.Namespace) -> List[str]:
    content: List[str] = []

    content.append('echo "Running on: $SLURM_NODELIST"')
    if args.array is not None:
        content.append('echo "Array task: $SLURM_ARRAY_TASK_ID"')

    if args.gpu > 0:
        content.append(f'export CUDA_HOME={args.cuda_home}')
        content.append('export CUDA_ROOT=$CUDA_HOME')
        content.append('export PATH=$CUDA_ROOT/bin:$PATH')
        content.append('export MANPATH=$CUDA_ROOT/doc/man:$MANPATH')
        content.append('export LD_LIBRARY_PATH=$CUDA_ROOT/lib64:$LD_LIBRARY_PATH')

    if args.python_location is not None:
        content.append(f'PYTHON_CMD={args.python_location}')

    return content


def dump_basescript(output: IO, parameters: List[str],
                    content: List[str]) -> None:
    print("#! /bin/sh", file=output)
    print(file=output)

    print("\n".join(parameters), file=output)
    print(file=output)
    print("\n".join(content), file=output)


def main() -> None:
    args = argparser()

    parameters: List[str] = setup_parameters(args)
    content: List[str] = setup_base_content(args)
    dump_basescript(sys.stdout, parameters, content)


if __name__ == '__main__':
    main()
