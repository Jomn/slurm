#! /bin/sh

#SBATCH -J finetune.multilingualbert.cased.calor_fquad_piaf
#SBATCH -t 1-00:00:00
##SLURM_ARRAY_ARGUMENT
#SBATCH -p gpu
#SBATCH --gpus 1
#SBATCH --constraint=cuda75
#SBATCH -o experiments/multilingualbert/finetune.multilingualbert.cased.calor_fquad_piaf.%N.%A_%a.out
#SBATCH -e experiments/multilingualbert/finetune.multilingualbert.cased.calor_fquad_piaf.%N.%A_%a.err

echo "Running on: $SLURM_NODELIST"
echo "Array task: $SLURM_ARRAY_TASK_ID"

export CUDA_HOME=/usr/local/cuda-11.0
export CUDA_ROOT=$CUDA_HOME
export PATH=$CUDA_ROOT/bin:$PATH
export MANPATH=$CUDA_ROOT/doc/man:$MANPATH
export LD_LIBRARY_PATH=$CUDA_ROOT/lib64:$LD_LIBRARY_PATH
PYTHON_CMD=/storage/raid1/homedirs/jeremy.auguste/.anaconda3/envs/qa-pytorch1.7/bin/python

train=data/calor_fquad_piaf-v1.json
output=experiments/multilingualbert/finetune.multilingualbert.cased.calor_fquad_piaf
cache_dir=cache/calor_fquad_piaf-v1

##BEGIN_REPLACE
$PYTHON_CMD finetune.py --input-cache-dir $cache_dir --num-train-epochs %epoch% --seed %seed% bert bert-base-multilingual-cased $train $output
##END_REPLACE
