#!/usr/bin/env python
# coding: utf-8

import argparse
import contextlib
import itertools
import logging
import os
import re
import sys
from dataclasses import dataclass
from typing import IO, Any, Dict, List, Optional, Tuple

import yaml


@dataclass
class BatchInfo:
    name: str
    stdout: str
    stderr: str
    var_names: List[str]
    var_tuples: List[Tuple[Any, ...]]
    constants: Dict[str, Any]
    simultaneous_jobs: Optional[int]
    walltime: Optional[str]
    qos: Optional[str]
    account: Optional[str]


@contextlib.contextmanager
def default_open(filename: Optional[str], mode: str = 'r'):
    fh: IO[Any]
    if filename is None or filename == '-':
        fh = sys.stdin if mode.startswith('r') else sys.stdout
    else:
        fh = open(filename, mode)

    try:
        yield fh
    finally:
        if fh not in [sys.stdout, sys.stdin]:
            fh.close()


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('template_file', type=argparse.FileType('r'))
    parser.add_argument('variables_file', type=argparse.FileType('r'),
                        help="YAML file which contains all the jobs to launch")
    parser.add_argument('--simultaneous-jobs', '-s', type=int,
                        help="Number of simultaneous jobs that slurm must launch at most")
    parser.add_argument('-o', '--output', default="-",
                        help="Output filename of the slurm batch file")
    parser.add_argument('-j', '--job-name', default="array_job",
                        help="Job name to give to slurm ")
    parser.add_argument('-t', '--walltime')
    parser.add_argument('-q', '--qos')
    parser.add_argument('-A', '--account')
    parser.add_argument('-O', '--slurm-stdout', default="slurm_outputs/{job_name}.%N.%A_%a.out",
                        help="Stdout filename pattern for slurm. Directories will be created if needed.")
    parser.add_argument('-E', '--slurm-stderr', default="slurm_outputs/{job_name}.%N.%A_%a.err",
                        help="Stderr filename pattern for slurm. Directories will be created if needed.")
    parser.add_argument('-l', '--logger', default='INFO',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help="Logging level: DEBUG, INFO (default), WARNING, ERROR")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.logger.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.logger))
    logging.basicConfig(level=numeric_level)

    return args


def load_variables(yaml_variables: Dict[str, Any]) -> Tuple[List[str], List[Tuple[Any, ...]]]:
    variable_names = []
    variables = []
    for name, values in yaml_variables.items():
        variable_names.append(name)
        variables.append(values)

    return variable_names, list(itertools.product(*variables))


def replace_lines(lines: List[str], var_names: List[str],
                  var_tuples: List[Tuple[Any, ...]],
                  constants: Dict[str, Any],
                  start_idx: int = 0) -> List[str]:
    replaced_lines: List[str] = []
    for tuple_idx, var_tuple in enumerate(var_tuples):
        new_lines = list(lines)

        for var_name, var in zip(var_names, var_tuple):
            for line_idx, line in enumerate(new_lines):
                new_lines[line_idx] = line.replace(f"%{var_name}%", str(var))

        if tuple_idx == 0:
            new_lines.insert(0, f"if [ $SLURM_ARRAY_TASK_ID -eq {tuple_idx} ]; then\n")
        else:
            new_lines.insert(0, f"elif [ $SLURM_ARRAY_TASK_ID -eq {tuple_idx} ]; then\n")

        if tuple_idx == len(var_tuples) - 1:
            new_lines.append("fi\n")

        replaced_lines.extend(new_lines)

    for name, value in constants.items():
        for line_idx, line in enumerate(replaced_lines):
            replaced_lines[line_idx] = line.replace(f"@{name}@", str(value))

    return replaced_lines


def replace_slurm_output(line: str, slurm_output: str,
                         job_name: str,
                         is_stderr: bool) -> str:
    directory_path = '/'.join(slurm_output.split('/')[:-1])
    os.makedirs(directory_path, exist_ok=True)

    slurm_output = slurm_output.replace("{job_name}", job_name.replace('/', '_'))

    flag = "-e" if is_stderr else "-o"
    return f"#SBATCH {flag} {slurm_output}\n"


def replace_template(ftemplate: IO, info: BatchInfo):
    lines: List[str] = []
    can_replace: bool = False
    to_replace_lines: List[str] = []
    for line in ftemplate:
        if line.startswith('##BEGIN_REPLACE'):
            can_replace = True
            continue
        if line.startswith('##END_REPLACE'):
            can_replace = False
            lines.extend(replace_lines(to_replace_lines,
                                       info.var_names, info.var_tuples,
                                       info.constants))
            continue
        if line.startswith("##SLURM_ARRAY_ARGUMENT"):
            simultaneous: str = "" if info.simultaneous_jobs is None else f"%{info.simultaneous_jobs}"
            lines.append(f"#SBATCH --array=0-{len(info.var_tuples)-1}{simultaneous}\n")
            continue

        if line.startswith("##SLURM_STDOUT"):
            lines.append(replace_slurm_output(line, info.stdout, info.name, False))
            continue
        if line.startswith("##SLURM_STDERR"):
            lines.append(replace_slurm_output(line, info.stderr, info.name, True))
            continue
        if line.startswith("##SLURM_JOB_NAME"):
            lines.append(f"#SBATCH -J {info.name}\n")
            continue
        if line.startswith("##SLURM_WALLTIME"):
            if info.walltime is None:
                raise RuntimeError("Can't replace SLURM_WALLTIME: no walltime given (-t)")
            lines.append(f"#SBATCH -t {info.walltime}\n")
            continue
        if line.startswith("##SLURM_ACCOUNT"):
            if info.account is None:
                raise RuntimeError("Can't replace SLURM_ACCOUNT: no account given (-A)")
            lines.append(f"#SBATCH -A {info.account}\n")
            continue
        if line.startswith("##SLURM_QOS"):
            if info.qos is None:
                raise RuntimeError("Can't replace SLURM_QOS: no qos given (-q)")
            lines.append(f"#SBATCH --qos={info.qos}\n")
            continue

        if not can_replace:
            lines.append(line)
            continue

        to_replace_lines.append(line)

    return lines


def validate_time_fmt(time: str):
    time_pattern = r"^([0-9]+-)?[0-2]?[0-9]:[0-6][0-9]:[0-6][0-9]$"
    if re.match(time_pattern, time) is None:
        raise RuntimeError("Walltime format is invalid.")


def create_batch(foutput: IO, ftemplate: IO, fyaml: IO,
                 job_name: str, slurm_stdout: str, slurm_stderr: str,
                 simultaneous_jobs: Optional[int],
                 walltime: Optional[str], qos: Optional[str],
                 account: Optional[str]):
    if walltime is not None:
        validate_time_fmt(walltime)

    yaml_config = yaml.load(fyaml, Loader=yaml.FullLoader)

    var_names, var_tuples = load_variables(yaml_config.get("variables", {}))
    logging.debug("Variable names: %s", var_names)
    logging.debug("Variable combinations: %s", var_tuples)

    batch_info = BatchInfo(job_name, slurm_stdout, slurm_stderr,
                           var_names, var_tuples,
                           yaml_config.get("constants", {}),
                           simultaneous_jobs, walltime,
                           qos, account)
    output_lines = replace_template(ftemplate, batch_info)

    for line in output_lines:
        print(line, end="", file=foutput)


def main():
    args = argparser()

    with default_open(args.output.replace("{job_name}", args.job_name.replace("/", "_")), 'w') as fout:
        create_batch(fout,
                     args.template_file, args.variables_file,
                     args.job_name, args.slurm_stdout, args.slurm_stderr,
                     args.simultaneous_jobs, args.walltime,
                     args.qos, args.account)


if __name__ == '__main__':
    main()
